<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    
<%if (request.getSession().getAttribute("User") == null)
{
	out.print("<script>window.location='Login.jsp'</script>");
    return ;
} %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
    <link href="lib/ligerUI/skins/Aqua/css/ligerui-all.css" rel="stylesheet" type="text/css" /> 
    <link href="lib/ligerUI/skins/Gray/css/all.css" rel="stylesheet" type="text/css" /> 
    <script src="lib/jquery/jquery-1.3.2.min.js" type="text/javascript"></script>
     <script src="lib/ligerUI/js/core/base.js" type="text/javascript"></script>
    <script src="lib/ligerUI/js/plugins/ligerDateEditor.js" type="text/javascript"></script>
    <script src="lib/ligerUI/js/plugins/ligerComboBox.js" type="text/javascript"></script>
    <script src="lib/ligerUI/js/plugins/ligerCheckBox.js" type="text/javascript"></script>
    <script src="lib/ligerUI/js/plugins/ligerButton.js" type="text/javascript"></script>
    <script src="lib/ligerUI/js/plugins/ligerDialog.js" type="text/javascript"></script>
    <script src="lib/ligerUI/js/plugins/ligerRadio.js" type="text/javascript"></script>
    <script src="lib/ligerUI/js/plugins/ligerSpinner.js" type="text/javascript"></script>
    <script src="lib/ligerUI/js/plugins/ligerTextBox.js" type="text/javascript"></script> 
    <script src="lib/ligerUI/js/plugins/ligerTip.js" type="text/javascript"></script>
    
    <script type="text/javascript">

        function btCancel_onclick() {
            parent.$.ligerDialog.close();
        }

    </script>
    <style type="text/css">
           body{ font-size:12px;}
        .l-table-edit {}
        .l-table-edit-td{ padding:4px;}
        .l-button-submit,.l-button-test{width:80px; float:left; margin-left:10px; padding-bottom:2px;}
        .l-verify-tip{ left:230px; top:120px;}
    </style>
</head>

<body style="padding:10px">
    <p>&nbsp;</p>
    <p>&nbsp;</p>
    <form id="form1"  method="post" action="ModPwd.do" >
    <div>
    
        <table style="width:300px;" align="center">
            
            <tr>
                <td style="text-align: right; height:36px; width:60px">
                    用户名：</td>
                <td>
                    <input type="text" ID="txtName" name="txtName" Width="200px" value="admin"/>
                </td>
            </tr>
            <tr>
                <td style="text-align: right; height:36px; width:60px">
                    密&nbsp;&nbsp;&nbsp;码：</td>
                <td>
                    <input type="Password" ID="txtPwd" name="txtPwd" Width="200px"/>
                </td>
            </tr>
            <tr>
                <td style="text-align: right; height:36px; width:60px">
                    新密码：</td>
                <td>
                	<input type="Password" ID="txtNewPwd" name="txtNewPwd" Width="200px"/>
                    
                </td>
            </tr>
            <tr>
                <td style="text-align: right; height:36px; width:60px">
                    确认密码：</td>
                <td>
                	<input type="Password" ID="txtNewPwd2" name="txtNewPwd2" Width="200px"/>
                    
                </td>
            </tr>
            <tr>
                <td colspan="2" style="text-align: center; height:48px">
                    <input type="submit" ID="btMod" value="确认" Width="90px" 
                         style="line-height24px"/>
&nbsp;&nbsp;<input id="btCancel" type="button" style="width:90px" value="取消" onclick="return btCancel_onclick()" /></td>
            </tr>
        </table>
    
    </div>
    </form>
</body>
</html>