<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="/mytaglib" prefix="cc"%>
<%@ page import="com.ErpCoreWeb.Common.Global" %>
<%@ page import="com.ErpCoreModel.Framework.CTable" %>
<%@ page import="com.ErpCoreModel.Framework.Util" %>
<%@ page import="com.ErpCoreModel.Framework.CBaseObject" %>
<%@ page import="com.ErpCoreModel.Framework.CBaseObjectMgr" %>
<%@ page import="com.ErpCoreModel.Base.CUser" %>
<%@ page import="java.util.UUID" %>

<%
CUser m_User = null;
if (request.getSession().getAttribute("User") == null)
{
    response.sendRedirect("../Login.jsp");
    return ;
}
m_User=(CUser)request.getSession().getAttribute("User");
if (!m_User.IsRole("管理员"))
{
	response.getWriter().print("没有管理员权限！");
	response.getWriter().close();
	return ;
}
String tid = request.getParameter("tid");
if (Global.IsNullParameter(tid))
{
    response.getWriter().close();
    return ;
}
CTable m_Table = (CTable)Global.GetCtx(this.getServletContext()).getTableMgr().Find(Util.GetUUID(tid));
pageContext.setAttribute("Table", m_Table);

String id = request.getParameter("id");
if (Global.IsNullParameter(id))
{
	response.getWriter().print("请选择记录！");
	response.getWriter().close();
}
UUID m_guidParentId=Util.GetEmptyUUID();
String ParentId = request.getParameter("ParentId");
if (!Global.IsNullParameter(ParentId))
    m_guidParentId = Util.GetUUID(ParentId);


CBaseObjectMgr m_BaseObjectMgr =Global.GetCtx(this.getServletContext()).FindBaseObjectMgrCache(m_Table.getCode(), m_guidParentId);
if (m_BaseObjectMgr == null)
{
    m_BaseObjectMgr = new CBaseObjectMgr();
    m_BaseObjectMgr.TbCode = m_Table.getCode();
    m_BaseObjectMgr.Ctx = Global.GetCtx(this.getServletContext());
    String sWhere = String.format(" id='%s'", id);
    m_BaseObjectMgr.GetList(sWhere);
}
CBaseObject m_BaseObject = m_BaseObjectMgr.Find(Util.GetUUID(id));
if (m_BaseObject == null)
{
	response.getWriter().print("请选择记录！");
    response.getWriter().close();
}
pageContext.setAttribute("BaseObject", m_BaseObject);
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <title></title>
    <link href="../lib/ligerUI/skins/Aqua/css/ligerui-all.css" rel="stylesheet" type="text/css" /> 
    <link href="../lib/ligerUI/skins/Gray/css/all.css" rel="stylesheet" type="text/css" />
    <script charset="utf-8" src="../kindeditor/examples/jquery.js"></script>
    <!--<script src="../lib/jquery/jquery-1.3.2.min.js" type="text/javascript"></script>-->
     <script src="../lib/ligerUI/js/core/base.js" type="text/javascript"></script>
    <script src="../lib/ligerUI/js/plugins/ligerForm.js" type="text/javascript"></script>
    <script src="../lib/ligerUI/js/plugins/ligerDateEditor.js" type="text/javascript"></script>
    <script src="../lib/ligerUI/js/plugins/ligerComboBox.js" type="text/javascript"></script>
    <script src="../lib/ligerUI/js/plugins/ligerCheckBox.js" type="text/javascript"></script>
    <script src="../lib/ligerUI/js/plugins/ligerButton.js" type="text/javascript"></script>
    <script src="../lib/ligerUI/js/plugins/ligerDialog.js" type="text/javascript"></script>
    <script src="../lib/ligerUI/js/plugins/ligerRadio.js" type="text/javascript"></script>
    <script src="../lib/ligerUI/js/plugins/ligerSpinner.js" type="text/javascript"></script>
    <script src="../lib/ligerUI/js/plugins/ligerTextBox.js" type="text/javascript"></script> 
    <script src="../lib/ligerUI/js/plugins/ligerTip.js" type="text/javascript"></script>
    <script src="../lib/jquery-validation/jquery.validate.min.js" type="text/javascript"></script> 
    <script src="../lib/jquery-validation/jquery.metadata.js" type="text/javascript"></script>
    <script src="../lib/jquery-validation/messages_cn.js" type="text/javascript"></script>
    
    <!--在线编辑器-->
	<link rel="stylesheet" href="../kindeditor/jquery-ui/css/smoothness/jquery-ui-1.9.2.custom.css" />
	<link rel="stylesheet" href="../kindeditor/themes/default/default.css" />
	<script charset="utf-8" src="../kindeditor/jquery-ui/js/jquery-ui-1.9.2.custom.js"></script>
	<script charset="utf-8" src="../kindeditor/kindeditor.js"></script>
	<script charset="utf-8" src="../kindeditor/lang/zh_CN.js"></script>

    <script type="text/javascript">

        function onSubmit() {
            document.getElementById("form1").submit();
        }
        function onCancel() {
            $.post(
                'Edit.do',
                {
                    tid: '<%=request.getParameter("tid") %>',
                    id: '<%=request.getParameter("id") %>',
                    Action: 'Cancel'
                },
                 function(data) {
                     if (data == "" || data == null) {
                         parent.onCancelEdit();
                         return true;
                     }
                     else {
                         $.ligerDialog.warn(data);
                         return false;
                     }
                 },
                 'text');
        } 
    </script>
    <style type="text/css">
           body{ font-size:12px;}
        .l-table-edit {}
        .l-table-edit-td{ padding:4px;}
        .l-button-submit,.l-button-test{width:80px; float:left; margin-left:10px; padding-bottom:2px;}
        .l-verify-tip{ left:230px; top:120px;}
    </style>
</head>
<body style="padding:10px">
    <iframe id="submitfrm" name="submitfrm" style="display: none"></iframe>
    <form id="form1" action="Edit.do" enctype="multipart/form-data" method="post" target="submitfrm">
    <input type="hidden" name="Action" value="PostData"/>
    <input type="hidden" name="tid" value="<%=request.getParameter("tid") %>"/>
    <input type="hidden" name="id" value="<%=request.getParameter("id") %>"/>
    <div >
        <cc:WindowRecordCtrl  table="${pageScope.Table }" baseObject="${pageScope.BaseObject }" />
    </div>
    </form>
</body>
</html>