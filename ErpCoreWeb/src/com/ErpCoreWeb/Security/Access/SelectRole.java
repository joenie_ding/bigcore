package com.ErpCoreWeb.Security.Access;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.ErpCoreModel.Base.CCompany;
import com.ErpCoreModel.Base.CRole;
import com.ErpCoreModel.Framework.CBaseObject;
import com.ErpCoreModel.Framework.Util;
import com.ErpCoreWeb.Common.Global;

/**
 * Servlet implementation class SelectRole
 */
@WebServlet("/SelectRole")
public class SelectRole extends HttpServlet {
	private static final long serialVersionUID = 1L;

	HttpServletRequest request;
	HttpServletResponse response;

    public CCompany m_Company = null;
    /**
     * @see HttpServlet#HttpServlet()
     */
    public SelectRole() {
        super();
        // TODO Auto-generated constructor stub
    }

    //初始化变量
    void initData()
    {
        if (request.getSession().getAttribute("User") == null)
        {
            try {
				response.sendRedirect("../Login.jsp");
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
            return ;
        }
        String B_Company_id = request.getParameter("B_Company_id");
		if (Global.IsNullParameter(B_Company_id))
			m_Company = Global.GetCtx(this.getServletContext())
					.getCompanyMgr().FindTopCompany();
		else
			m_Company = (CCompany) Global.GetCtx(this.getServletContext())
					.getCompanyMgr().Find(Util.GetUUID(B_Company_id));
	
    }
	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doProc(request,response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doProc(request,response);
	}
	void doProc(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
	{
		this.request=request;
		this.response=response;

		response.setContentType("text/html;charset=UTF-8");
		request.setCharacterEncoding("UTF-8");

		//初始化变量
	    initData();
	    //
        String Action = request.getParameter("Action");
        if (Action == null) Action = "";

        if (Action.equalsIgnoreCase("GetData"))
        {
            GetData();
            return ;
        }
		
	}

    void GetData()
    {
    	int page = Integer.valueOf(request.getParameter("page"));
		int pageSize = Integer.valueOf(request.getParameter("pagesize"));


        String sData = "";
        List<CBaseObject> lstObj = m_Company.getRoleMgr().GetList();
        
        int totalPage = lstObj.size() % pageSize == 0 ? lstObj.size() / pageSize : lstObj.size() / pageSize + 1; // 计算总页数

        int index = (page - 1) * pageSize; // 开始记录数  
        for (int i = index; i < pageSize + index && i < lstObj.size(); i++)
        {
            CRole role = (CRole)lstObj.get(i);

            sData += String.format("{ \"id\": \"%s\",\"Name\":\"%s\" },"
                , role.getId().toString(), role.getName());

        }
        if(sData.length()>0 && sData.endsWith(","))
        	sData = sData.substring(0, sData.length()-1);
        sData = "[" + sData + "]";
        String sJson = String.format("\"Rows\":%s,\"Total\":\"%d\"}"
            , sData, lstObj.size());

		try {
			response.getWriter().print(sJson);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    }
}
