﻿// File:    COrderDetail.java
// Author:  甘孝俭
// email:   ganxiaojian@hotmail.com 
// QQ:      154986287
// http://www.8088net.com
// 协议声明：本软件为开源系统，遵循国际开源组织协议。任何单位或个人可以使用或修改本软件源码，
//          可以用于作为非商业或商业用途，但由于使用本源码所引起的一切后果与作者无关。
//          未经作者许可，禁止任何企业或个人直接出售本源码或者把本软件作为独立的功能进行销售活动，
//          作者将保留追究责任的权利。
// Created: 2015/12/6 10:23:33
// Purpose: Definition of Class COrderDetail

package com.ErpCoreModel.Store;

import java.lang.reflect.Type;
import java.util.Date;
import java.util.HashMap;
import java.util.UUID;
import com.ErpCoreModel.Framework.CBaseObject;
import com.ErpCoreModel.Framework.Util;
import com.ErpCoreModel.Framework.CValue;

public class COrderDetail extends CBaseObject
{

    public COrderDetail()
    {
        TbCode = "DD_OrderDetail";
        ClassName = "com.ErpCoreModel.Store.COrderDetail";

    }

    
        public String getSpecification()
        {
            if (m_arrNewVal.containsKey("specification"))
                return m_arrNewVal.get("specification").StrVal;
            else
                return "";
        }
        public void setSpecification(String value)
        {
            if (m_arrNewVal.containsKey("specification"))
                m_arrNewVal.get("specification").StrVal = value;
            else
            {
                CValue val = new CValue();
                val.StrVal = value;
                m_arrNewVal.put("specification", val);
            } 
       }
        public String getColor()
        {
            if (m_arrNewVal.containsKey("color"))
                return m_arrNewVal.get("color").StrVal;
            else
                return "";
        }
        public void setColor(String value)
        {
            if (m_arrNewVal.containsKey("color"))
                m_arrNewVal.get("color").StrVal = value;
            else
            {
                CValue val = new CValue();
                val.StrVal = value;
                m_arrNewVal.put("color", val);
            } 
       }
        public double getNum()
        {
            if (m_arrNewVal.containsKey("num"))
                return m_arrNewVal.get("num").DoubleVal;
            else
                return 0;
        }
        public void setNum(double value)
        {
            if (m_arrNewVal.containsKey("num"))
                m_arrNewVal.get("num").DoubleVal = value;
            else
            {
                CValue val = new CValue();
                val.DoubleVal = value;
                m_arrNewVal.put("num", val);
            }
        }
        public double getPrice()
        {
            if (m_arrNewVal.containsKey("price"))
                return m_arrNewVal.get("price").DoubleVal;
            else
                return 0;
        }
        public void setPrice(double value)
        {
            if (m_arrNewVal.containsKey("price"))
                m_arrNewVal.get("price").DoubleVal = value;
            else
            {
                CValue val = new CValue();
                val.DoubleVal = value;
                m_arrNewVal.put("price", val);
            }
        }
        public double getDiscount()
        {
            if (m_arrNewVal.containsKey("discount"))
                return m_arrNewVal.get("discount").DoubleVal;
            else
                return 0;
        }
        public void setDiscount(double value)
        {
            if (m_arrNewVal.containsKey("discount"))
                m_arrNewVal.get("discount").DoubleVal = value;
            else
            {
                CValue val = new CValue();
                val.DoubleVal = value;
                m_arrNewVal.put("discount", val);
            }
        }
        public UUID getDD_Order_id()
        {
            if (m_arrNewVal.containsKey("dd_order_id"))
                return m_arrNewVal.get("dd_order_id").GuidVal;
            else
                return Util.GetEmptyUUID();
        }
        public void setDD_Order_id(UUID value)
        {
            if (m_arrNewVal.containsKey("dd_order_id"))
                m_arrNewVal.get("dd_order_id").GuidVal = value;
            else
            {
                CValue val = new CValue();
                val.GuidVal = value;
                m_arrNewVal.put("dd_order_id", val);
            }
        }
        public int getState()
        {
            if (m_arrNewVal.containsKey("state"))
                return m_arrNewVal.get("state").IntVal;
            else
                return 0;
        }
        public void setState(int value)
        {
            if (m_arrNewVal.containsKey("state"))
                m_arrNewVal.get("state").IntVal = value;
            else
            {
                CValue val = new CValue();
                val.IntVal = value;
                m_arrNewVal.put("state", val);
            }
        }
        public UUID getSP_Product_id()
        {
            if (m_arrNewVal.containsKey("sp_product_id"))
                return m_arrNewVal.get("sp_product_id").GuidVal;
            else
                return Util.GetEmptyUUID();
        }
        public void setSP_Product_id(UUID value)
        {
            if (m_arrNewVal.containsKey("sp_product_id"))
                m_arrNewVal.get("sp_product_id").GuidVal = value;
            else
            {
                CValue val = new CValue();
                val.GuidVal = value;
                m_arrNewVal.put("sp_product_id", val);
            }
        }
}
