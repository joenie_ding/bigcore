﻿// File:    CProduct.java
// Author:  甘孝俭
// email:   ganxiaojian@hotmail.com 
// QQ:      154986287
// http://www.8088net.com
// 协议声明：本软件为开源系统，遵循国际开源组织协议。任何单位或个人可以使用或修改本软件源码，
//          可以用于作为非商业或商业用途，但由于使用本源码所引起的一切后果与作者无关。
//          未经作者许可，禁止任何企业或个人直接出售本源码或者把本软件作为独立的功能进行销售活动，
//          作者将保留追究责任的权利。
// Created: 2015/12/6 10:23:34
// Purpose: Definition of Class CProduct

package com.ErpCoreModel.Store;

import java.lang.reflect.Type;
import java.util.Date;
import java.util.HashMap;
import java.util.UUID;
import com.ErpCoreModel.Framework.CBaseObject;
import com.ErpCoreModel.Framework.Util;
import com.ErpCoreModel.Framework.CValue;

public class CProduct extends CBaseObject
{

    public CProduct()
    {
        TbCode = "SP_Product";
        ClassName = "com.ErpCoreModel.Store.CProduct";

    }

    
        public String getName()
        {
            if (m_arrNewVal.containsKey("name"))
                return m_arrNewVal.get("name").StrVal;
            else
                return "";
        }
        public void setName(String value)
        {
            if (m_arrNewVal.containsKey("name"))
                m_arrNewVal.get("name").StrVal = value;
            else
            {
                CValue val = new CValue();
                val.StrVal = value;
                m_arrNewVal.put("name", val);
            } 
       }
        public String getCode()
        {
            if (m_arrNewVal.containsKey("code"))
                return m_arrNewVal.get("code").StrVal;
            else
                return "";
        }
        public void setCode(String value)
        {
            if (m_arrNewVal.containsKey("code"))
                m_arrNewVal.get("code").StrVal = value;
            else
            {
                CValue val = new CValue();
                val.StrVal = value;
                m_arrNewVal.put("code", val);
            } 
       }
        public String getUnit()
        {
            if (m_arrNewVal.containsKey("unit"))
                return m_arrNewVal.get("unit").StrVal;
            else
                return "";
        }
        public void setUnit(String value)
        {
            if (m_arrNewVal.containsKey("unit"))
                m_arrNewVal.get("unit").StrVal = value;
            else
            {
                CValue val = new CValue();
                val.StrVal = value;
                m_arrNewVal.put("unit", val);
            } 
       }
        public String getBrand()
        {
            if (m_arrNewVal.containsKey("brand"))
                return m_arrNewVal.get("brand").StrVal;
            else
                return "";
        }
        public void setBrand(String value)
        {
            if (m_arrNewVal.containsKey("brand"))
                m_arrNewVal.get("brand").StrVal = value;
            else
            {
                CValue val = new CValue();
                val.StrVal = value;
                m_arrNewVal.put("brand", val);
            } 
       }
        public String getDetail()
        {
            if (m_arrNewVal.containsKey("detail"))
                return m_arrNewVal.get("detail").StrVal;
            else
                return "";
        }
        public void setDetail(String value)
        {
            if (m_arrNewVal.containsKey("detail"))
                m_arrNewVal.get("detail").StrVal = value;
            else
            {
                CValue val = new CValue();
                val.StrVal = value;
                m_arrNewVal.put("detail", val);
            } 
       }
        public String getFactory()
        {
            if (m_arrNewVal.containsKey("factory"))
                return m_arrNewVal.get("factory").StrVal;
            else
                return "";
        }
        public void setFactory(String value)
        {
            if (m_arrNewVal.containsKey("factory"))
                m_arrNewVal.get("factory").StrVal = value;
            else
            {
                CValue val = new CValue();
                val.StrVal = value;
                m_arrNewVal.put("factory", val);
            } 
       }
}
